package automation_week3;

import org.openqa.selenium.support.ui.Select;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;

import java.util.ArrayList;
import java.util.List;


public class StateSelect extends BaseTestClass {

    @Test
    public void selectStateDropDown(){

        driver.get("https://www.mediacollege.com/internet/samples/html/state-list.html");

        WebElement stateElement = driver.findElement(By.name("state"));
        Select stateDD = new Select(stateElement);

        stateDD.selectByValue("ZZ");

        Assert.assertEquals("None", stateDD.getFirstSelectedOption().getText());

        List<WebElement> stateList = stateDD.getOptions();
        for (WebElement currentState:stateList) {
            if (currentState.getText().endsWith("k"))
                System.out.println(currentState.getText());

//            Assert.assertEquals(stateList.size(), 2);
        }
        List<String> selectedState = new ArrayList<>();
        selectedState.add("New York");
        selectedState.add("New Brunswick");

        Assert.assertEquals(2, selectedState.size());
 }
}
