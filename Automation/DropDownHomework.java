package automation_week2;

import com.gargoylesoftware.htmlunit.activex.javascript.msxml.XMLDOMAttribute;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import teme.util.ta_utils.BaseTestClass;

import java.util.List;

/*      1. Navigate to the site:
        https://www.mediacollege.com/internet/samples/html/state-list.html
        2. Check that Arizona is present.
        3. Check if the list contains at least one state ending with “k”.*/
public class DropDownHomework extends BaseTestClass {

    @Test
    public void dropDownState() {
        driver.get("https://www.mediacollege.com/internet/samples/html/state-list.html");
        WebElement stateElement = driver.findElement(By.name("state"));
        Select stateDropDown = new Select(stateElement);

        //2. Check that Arizona is present.
        List<WebElement> dropDownOptions = stateDropDown.getOptions();
        boolean isArizonafound= false;
        for (WebElement currentStatefromDD:dropDownOptions) {
            if (currentStatefromDD.getText().equals("Arizona")) {
                isArizonafound = true;
                break;
            }
        }
        Assert.assertTrue("Arizona is not part of the list", isArizonafound);

        // 3. Check if the list contains at least one state ending with “k”.
        boolean existsStateEndingWithK=false;
        for (WebElement currentStatefromDD:dropDownOptions) {
            if( currentStatefromDD.getText().endsWith("k")){
                existsStateEndingWithK=true;
                break;
            }

        }
        Assert.assertTrue("There is no country ending with k", existsStateEndingWithK);
    }
}
