package automation_week2;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.junit.Test;
import teme.util.ta_utils.BaseTestClass;
import java.util.List;

public class MultipleSelectDemo extends BaseTestClass {

    @Test
    public void multipleSelectDDTest() {
        driver.get("http://output.jsbin.com/osebed/2");
        Select fruitsDD = new Select(driver.findElement(By.id("fruits"))); //type=WebElement

        //cleanup
        fruitsDD.deselectAll();

        //select using the option by value
        fruitsDD.selectByValue("grape");

        //select using the option by index
        fruitsDD.selectByIndex(2); //starts with 0 ->orange

        //verify if selected values are grape and orange
        List<WebElement> selectedFruits = fruitsDD.getAllSelectedOptions(); //2 (grape (1) and orange(0))

        Assert.assertEquals("Grape is not on the list", "Grape", selectedFruits.get(1).getText());
        Assert.assertEquals("Orange is not on the list", "Orange", selectedFruits.get(0).getText());
        //se mai poate face un assert cu metoda size pentru a verifica ca lista are dimensiunea pe care ne asteptam sa o aiba
        Assert.assertEquals(2, selectedFruits.size());

//        fruitsDD.deselectByValue("grape"); //deselecteaza pe baza codului HTML-value
        fruitsDD.deselectByVisibleText("Grape"); //deselecteaza pe baza textului din UI

        Assert.assertEquals("Orange", selectedFruits.get(0).getText());
        Assert.assertEquals(2, selectedFruits.size());

        //cleanup pentru a nu afecta testele ce vor fi rulate dupa
        fruitsDD.deselectAll();



    }
}
