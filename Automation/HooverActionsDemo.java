package automation_week3;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import teme.util.ta_utils.BaseTestClass;

public class HooverActionsDemo extends BaseTestClass {

    @Test
    public void hooverTest(){
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/");

        WebElement titlePage = driver.findElement(By.linkText("My CMS"));

        String initialColor = titlePage.getCssValue("color"); //gri inchis
        //gri inchis #303030 -> rgba-> rgba(48, 48, 48, 1)
        String expectedColor  = convertCodesToString("48, 48, 48", "1"); // convert to String

        System.out.println("The initial color is: " + initialColor);

        Assert.assertEquals(expectedColor, initialColor);
        //(are, 10, teme)-> cursantul are 10 teme.

        Actions hover = new Actions(driver);
        Action mouseOver = hover.moveToElement(titlePage).build();
        mouseOver.perform();

        String hoverColor = titlePage.getCssValue("color");//initial after hover
        //rgba(238, 51, 51, 1)
        String expectedHoverColor = convertCodesToString("238, 51, 51", "1");

        Assert.assertEquals(expectedHoverColor, hoverColor);
    }

    private String convertCodesToString(String colorCodes, String number) {
        return "rgba(" + colorCodes + ", " + number + ")";
    }
}
