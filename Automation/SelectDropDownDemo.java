package automation_week2;

import org.openqa.selenium.support.ui.Select;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;

import java.util.List;
/*  1. Verify that the default selected option is "Country...".
    2. Check that Romania is present.
    3. Check that the last option is Zimbabwe.
    4. Check that there are no countries starting with X.
 */

public class SelectDropDownDemo extends BaseTestClass {

    @Test
    public void selectSingleDropDownPractice() {
        driver.get("https://www.mediacollege.com/internet/samples/html/country-list.html");

        WebElement countryElement = driver.findElement(By.name("country")); //am luat doar elementul
        Select countryDropDown = new Select(countryElement); //ne facem un obiect de tip select

        //1.Verify that the default selected option is "Country...".
        String defaultValue = countryDropDown.getFirstSelectedOption().getText(); //metoda pe care o putem accesa daca avem declarat drop down-ul
        Assert.assertEquals("The default value is not correct", "Country...", defaultValue); //se face assert pe default values

        //2. Check that Romania is present.
        // vom face o validare de tip for each
        //Get the list of countries
        List<WebElement> dropDownOptions = countryDropDown.getOptions(); //modalitatae prin care se poate lua text din UI
        //Creeam o variabila de tip boolean care sa stocheze rezultatul for each-ului
        boolean isRomaniaFound = false; //variabila stocheaza raspunsul intrebarii; ii dam o valoare default
        for (WebElement currentCountryFromDD:dropDownOptions) {
            if(currentCountryFromDD.getText().equals("Romania")){
                isRomaniaFound = true;
                break;
            }
        }
        Assert.assertTrue("Romania is not part of the list", isRomaniaFound); //acest mesaj va fi afisat daca boolean este false(Romania nu este pe lista)

        // 3. Check that the last option is Zimbabwe. - vom lua indexul pentru optiunea respectiva printr-o variabila de tip int
        int lastDDIndex = dropDownOptions.size() -1; //index-ul optiunilor din dropdown
        String lastCountryName = dropDownOptions.get(lastDDIndex).getText();

        Assert.assertEquals("Is not on the list", "Zimbabwe", lastCountryName);

        //  4. Check that there are no countries starting with X.

        boolean existsCountryStartsWithX = false;
        for (WebElement currentCountry:dropDownOptions) {
            if(currentCountry.getText().startsWith("X")){
                existsCountryStartsWithX = true;
                break;
            }
        }
        Assert.assertFalse("There are countries starting with X", existsCountryStartsWithX);
        //Assert.assertTrue("There are countries starting with X", !existsCountryStartsWithX);
    }
}
