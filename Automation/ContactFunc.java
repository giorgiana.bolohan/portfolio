package automation_week2;

import org.junit.Test;
import org.openqa.selenium.support.ui.Select;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;

import java.util.concurrent.TimeUnit;

public class ContactFunc extends BaseTestClass {
    @Test
    public void ContactFunctionalityTest() {
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/contact/");

        WebElement yourNameInput = driver.findElement(By.xpath("//*[@name= 'your-name']"));
        yourNameInput.sendKeys("Contact1996");

        WebElement yourEmailInput = driver.findElement(By.xpath("//*[@name = 'your-email']"));
        yourEmailInput.sendKeys("tester1996@gmail.com");

        WebElement yourPhoneInput = driver.findElement(By.xpath("//*[@name = 'tel-437']"));
        yourPhoneInput.sendKeys("1234567890");

        WebElement subjectInput = driver.findElement(By.xpath("//*[@name = 'your-subject']"));
        subjectInput.sendKeys("Information about testing types");

        WebElement messageInput = driver.findElement(By.xpath("//*[@name = 'your-message']"));
        messageInput.sendKeys("Can I have some piece of information about testing types?");

        Select topicDD = new Select(driver.findElement(By.name("Select")));
        topicDD.selectByValue("Technical details");
//        List<WebElement> topics = topicDD.getAllSelectedOptions();
        Assert.assertEquals("Error", "Technical details", topicDD.getFirstSelectedOption().getText());

        WebElement friendsButton = driver.findElement(By.xpath("//*[text() = 'Friends']"));
        friendsButton.click();

        WebElement professionAreaButton = driver.findElement(By.xpath("//*[text() = 'Sales']"));
        professionAreaButton.click();

        WebElement send = driver.findElement(By.xpath("//input[@type= 'submit']"));
        send.click();

//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        WebElement errorMessage = driver.findElement(By.xpath("//div[contains(text(), 'Please check and try again'')]"));
//        String actualErrorMessage = errorMessage.getText();


        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement errorMessage2 = driver.findElement(By.xpath("//div[contains(text(), 'Please try again later.')]"));
        String error2 =errorMessage2.getText();
        Assert.assertEquals("The error is not displayed", "There was an error trying to send your message. Please try again later.", error2);
//
    }

}


