package automation_week2;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;

import java.security.Key;

public class ManipulationDemo extends BaseTestClass {

    @Test

    public void verifyIfLoginWorksAsExpectedWithValidCredentials() {
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

        WebElement usernameTextbox = driver.findElement(By.id("user_login")); //interactionam cu text-box-ul pentru log in
        usernameTextbox.clear(); //se goleste textbox-ul
        usernameTextbox.sendKeys("cursant1"); //se introduce username-ul
//        pentru partea de senKeys
//        usernameTextbox.sendKeys(Keys.CONTROL + "A");
//        usernameTextbox.sendKeys(Keys.ENTER);
//        usernameTextbox.sendKeys(Keys.TAB);


        WebElement passwordTextbox = driver.findElement(By.id("user_pass")); //se face acelasi lucru si pentru parola
        passwordTextbox.clear();
        passwordTextbox.sendKeys("Tester123");

        WebElement loginButton = driver.findElement(By.id("wppb-submit")); //interactionam cu butonul de submit
        loginButton.click(); //se face click pe butonul de submit

        WebElement logoutButton = driver.findElement(By.xpath("//a[text()='Logout']"));
        Assert.assertEquals("Invalid login!!", "Logout", logoutButton.getText());
    }

    @Test
    public void verifyIfRegisterWorksByFillinfTheMandatoryFields () {
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/register/");
        WebElement username = driver.findElement(By.id("username"));
        username.sendKeys("GiorgianaBolo");

        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("giorgianabolo@test.ro");

        WebElement password = driver.findElement(By.id("passw1"));
        password.sendKeys("Wantsome12345");

        WebElement password2 = driver.findElement(By.id("passw2"));
        password2.sendKeys("Wantsome12345");

        WebElement registerButton = driver.findElement(By.id("register"));
        registerButton.submit(); // submit= click /actiunea de inregistrare

        WebElement registerSuccesfullyMessage = driver.findElement(By.xpath("//p[contains(text(), 'successfully created!')]"));
        //având linia anterioară la bază, se pasează WebElement-ul registerSuccesfullyEssage într-un String pe care să îl stocăm într-o variabilă normală
        String actualRegisterMessage = registerSuccesfullyMessage.getText(); //variabila locala utilizata doar in interiorul @Test

        Assert.assertEquals("The register is not working!", "The account GiorgianaB has been successfully created!", actualRegisterMessage);


    }


}
