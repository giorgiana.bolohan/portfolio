package automation_week3;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;

public class Practice extends BaseTestClass {

    @Test
    public void verifySearchFunctionality(){
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/");

        WebElement searchBar = driver.findElement(By.className("search-field"));
        searchBar.clear();
        searchBar.sendKeys("cafea");
        WebElement searchButton = driver.findElement(By.xpath("//button[@class='search-submit']"));
        searchButton.click();
        WebElement errorMessage = driver.findElement(By.className("page-title"));
        Assert.assertEquals("The error message is not displayed", "Nothing Found", errorMessage.getText());
    }
}
