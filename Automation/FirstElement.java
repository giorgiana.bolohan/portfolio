package automation_week1;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;


public class FirstElement extends BaseTestClass {
    @Test
    public void MyCns(){
        //accesam link-ul
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/");
        WebElement logo = driver.findElement(By.id("logo")); //identifica tot elementul
        //actual result
        String logoText = logo.getText(); //se declara o variabila de tip String și se ia text-ul apeland logo-ul
        System.out.println(logoText); // este o verificare pentru a printa valoarea
//luam elementul site-description ca mai sus  doar ca il vom cauta dupa clasa
        WebElement welcomeMessage = driver.findElement(By.className("site-description")); // site-description este un element unic de care ne putem lega
        String actualResult = welcomeMessage.getText();
        String expectedResult = "Practice website for testing sessions";

        Assert.assertEquals(expectedResult, actualResult); //partea de verificare

    }
}
