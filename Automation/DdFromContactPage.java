package automation_week2;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.junit.Test;
import org.openqa.selenium.By;
import teme.util.ta_utils.BaseTestClass;

import java.util.List;

public class DdFromContactPage extends BaseTestClass {

    @Test
    public void myDd() {

        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/contact/");

        Select topicDD = new Select(driver.findElement(By.name("Select")));

        topicDD.selectByValue("Technical details");

        List<WebElement> topics = topicDD.getAllSelectedOptions(); //

        Assert.assertEquals("error", "Technical details", topicDD.getFirstSelectedOption().getText());
    }
}
