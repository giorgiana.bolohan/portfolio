package automation_week2;
/*1. Navigate to the site: https://thinking-tester-contact-list.herokuapp.com/
        2. Login into the app.
        3. Click on the button add a new contact.
        4. Fill the first name, last name, email, city, country.
        5. Click on submit button.
        6. Assert that the contact is created.*/

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;


import java.time.Duration;
import java.util.concurrent.TimeUnit;


public class DropDownHomework2 extends BaseTestClass {

@Test
public void ContactListApp() {
        driver.get("https://thinking-tester-contact-list.herokuapp.com/");

        WebElement emailInput = driver.findElement(By.id("email"));
        emailInput.sendKeys("giorgianabolohan@yahoo.com");

        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("test12345");

        WebElement submitButton = driver.findElement(By.id("submit"));
        submitButton.click();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement addContactButton = driver.findElement(By.id("add-contact"));
        addContactButton.click();

        WebElement firstName = driver.findElement(By.id("firstName"));
        firstName.sendKeys("Thomas");

        WebElement lastName = driver.findElement(By.id("lastName"));
        lastName.sendKeys("Foster");

        WebElement emailContact = driver.findElement(By.id("email"));
        emailContact.sendKeys("thomasfoster@test.com");

        WebElement cityContact = driver.findElement(By.id("city"));
        cityContact.sendKeys("Charlotte");

        WebElement countryContact = driver.findElement(By.id("country"));
        countryContact.sendKeys("USA");

        WebElement submitButtonAddContact = driver.findElement(By.id("submit"));
        submitButtonAddContact.click();


        WebElement currentPageTitle = driver.findElement(By.xpath("//*[text()= 'Contact List']"));
        String expectedTitle = "Contact List";

        Assert.assertEquals( "The Contact List page is not displayed", expectedTitle, currentPageTitle.getText());

}
}
