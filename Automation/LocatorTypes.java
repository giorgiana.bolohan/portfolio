package automation_week1;

import com.gargoylesoftware.htmlunit.activex.javascript.msxml.XMLDOMElement;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import teme.util.ta_utils.BaseTestClass;

import java.util.List;

public class LocatorTypes extends BaseTestClass {
    @Test
    public void findLocatorById() {
        //accesare site
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");
        //identificare element web dupa un anumit tip de locator - By Id
        WebElement checkBox = driver.findElement(By.id("rememberme"));
        //verificare daca check box este selectat sau nu, aplicand isSelected pe elementul checkbox
        Assert.assertFalse("Verify if check box is selected", checkBox.isSelected());
        //actiune (se face click pe element)
        checkBox.click();
        //verificarea rezultatului actiunii
        Assert.assertTrue(checkBox.isSelected());

    }

    @Test

    public void findLocatorByClass() {
        //accesare site
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");
        //identificare element
        WebElement searchBar = driver.findElement(By.className("search-field"));
        //verificare
        String actualPlaceholder = searchBar.getAttribute("placeholder");

//        String expectedPlaceholder = "Search …";

        Assert.assertEquals("Search …", actualPlaceholder);

    }

    @Test

    public void findElementbyName() {

        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

        WebElement inputUsername = driver.findElement(By.name("log"));

        WebElement inputPassword = driver.findElement(By.name("pwd"));

        Assert.assertEquals("text", inputUsername.getAttribute("type"));
        Assert.assertEquals("password", inputPassword.getAttribute("type"));

    }

    @Test

    public void findElementbyLinkTest(){
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/contact/");
        // identificam elementul web dorit - pe care vom face interogarea

        WebElement linkDecember = driver.findElement(By.linkText("December 2019"));

        //facem verificare de care avem nevoie - verificam link-ul din spatele textului afisat pe site
        Assert.assertEquals("https://testare-manuala.locdejoacapentruitsti.com/blog/2019/12/", linkDecember.getAttribute("href"));

    }

    @Test

    public void findElementByPartialLinkText() //sa identificam valorile acelor elemente a caror valoare o avem partial

    { driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/contact/");

        //caut elementul care contine "ber2018" - find element identifica primul element gasit care contine "ber 2018"
        WebElement firstBer2018 = driver.findElement(By.partialLinkText("ber 2018"));

        Assert.assertEquals("December 2018", firstBer2018.getText());

        List<WebElement> listaLinkuri = driver.findElements(By.partialLinkText("ber 2018"));

        int expectedResults = 3;

        Assert.assertEquals(expectedResults, listaLinkuri.size());

        Assert.assertEquals("December 2018", listaLinkuri.get(0).getText());
        Assert.assertEquals("November 2018", listaLinkuri.get(1).getText());
        Assert.assertEquals("October 2018", listaLinkuri.get(2).getText() );


    }

    @Test

    public void findElementXpathWithName ()

    {
        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

        WebElement emailInput = driver.findElement(By.xpath("//*[@name='log']"));

        Assert.assertEquals("", emailInput.getAttribute("value"));

    }

    @Test
    public void findElementByXpathWithClassName() {

        driver.get("https://testare-manuala.locdejoacapentruitsti.com/blog/login/");

        WebElement searchBox = driver.findElement(By.xpath("//*[@class='search-field']"));

        String actualPlaceholder = searchBox.getAttribute("placeholder");

        Assert.assertEquals("Search …", actualPlaceholder);
    }


    }

