package java_week4;

import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
    public static void main(String[] args) {
        // Set<String> orase = new HashSet<>();
//        Set<String> orase = new LinkedHashSet<>();

        // lista sortata A - Z / 1 - 10000
        Set<String> orase = new TreeSet<>();
        // streams  Java 8
//        orase.stream().findFirst();

        // add elements to set
        orase.add("Iasi");
        orase.add("Cluj");
        orase.add("Vaslui");
        orase.add("Bacau");

        System.out.println(orase);

        // add duplicates
        orase.add("Bacau");
        System.out.println(orase);

//        orase.remove("Bacau");
//        orase.clear();
        orase.size(); // 0 // 3

        // for each on set / list
        for (String orasName : orase) {
            if (orasName.contains("Vaslui"))
                System.out.println("Orasele din for sunt: " + orasName);
        }
    }
}
