package java_week2;


public class IfDemo {
    public static void main(String[] args) {
        boolean condition = false;
        if (condition)
        {
            System.out.println("Am trecut prin if");
        }
        else
        {
            System.out.println("Am trecut prin else");
        }

        int number = 4;
        if (number >  3)
        {
            System.out.println("Am trecut prin if");
        }
        else
        {
            System.out.println("Am trecut prin else");
        }

        boolean ploua = true;
        if(ploua){
            System.out.println("Nu ud gazonul");
        }
        else {
            System.out.println("Ud gazonul");
        }

        String culoareSemafor = "rosu";
        if (culoareSemafor == "rosu") {
            System.out.println("Oprim");
        }
        else {
            System.out.println("Conducem");
        }


        boolean locDeJoaca = true;
        int pretPerNoapte=300;
        if(locDeJoaca && pretPerNoapte<400) {
            System.out.println("Fac rezervare");
            System.out.println("Si iau si bunicii");
        }
        else{
            System.out.println("Mai caut");
        }
    }
}
