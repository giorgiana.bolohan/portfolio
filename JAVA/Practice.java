package java_week4;

import java.util.*;

public class Practice {
    /* 1.Write a Java Method to insert elements (colors - 4) into a linked list at the first and last positions. */

    public static void colorList() {
        // create an empty linked list
        LinkedList<String> l_list = new LinkedList<String>();
        // use add() method to add values in the linked list
        l_list.add("Red");
        l_list.add("Green");
        l_list.add("Black");
        System.out.println("Original linked list:" + l_list);
        // Add an element at the beginning
        l_list.addFirst("White");

        // Add an element at the end of list
        l_list.addLast("Pink");
        System.out.println("Final linked list:" + l_list);

    }

    /* 2. Write a Java Method to iterate through all elements in a sorted set (with names).
          Print only the values that are starting with “A”.
          Display a message in case there are names that are not starting with "A". */

    public static void sortedSet() {
        TreeSet<String> sortedSet = new TreeSet<>();
        sortedSet.add("Alina");
        sortedSet.add("Diana");
        sortedSet.add("Silviu");
        sortedSet.add("Alex");
        sortedSet.add("Darius");

        // Print the tree set
        for (String name : sortedSet) {
            if(name.startsWith("A")) {
                System.out.println(name);
            } else
                System.out.println("There is no name that respect the user validation.");
        }
    }

    /* 3. Write a Java Method to find maximum element in ArrayList. */
    public static void maxValue() {
        List<Integer> num_list = new ArrayList();
        num_list.add(80);
        num_list.add(18);
        num_list.add(48);
        num_list.add(37);
        num_list.add(24);
        Object max_num = Collections.max(num_list);
        System.out.println("Maximum Element ArrayList is : " + max_num);
    }

    /* 4. Create a list with ages (populated with 5 values).
          Create a Java method that will calculate the average of the values existing on the list.
     */

    public static void averageAge() {
        List<Integer> ages = new ArrayList<>();
        ages.add(28);
        ages.add(30);
        ages.add(45);
        ages.add(29);
        ages.add(35);

        int sum = 0;
        for (Integer age : ages) {
            sum += age;
        }

        System.out.println("The average is: " + (double) sum/ages.size());

    }

    public static void main(String[] args) {
        colorList();
        sortedSet();
        maxValue();
        averageAge();
    }
}
