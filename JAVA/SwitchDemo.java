package java_week2;

public class SwitchDemo {
    public static void main(String[] args) {
//        switch('B'){
//            case 'A':
//                System.out.println("Excellent");
//                break;
//            case 'B':
//            case 'C':
//                System.out.println("Well done");
//                break;
//            case 'D':
//                System.out.println("You passed");
//                break;
//            case 'F':
//                System.out.println("Try again");
//                break;
//            default:
//                System.out.println("invalid grade");
//        }

        int ziuaSaptamanii = 3;
        switch (ziuaSaptamanii){
            case 1:
                System.out.println("Luni");
            break;
            case 2:
                System.out.println("Marti");
                break;
            case 3:
                System.out.println("Miercuri");
                break;
            default:
                System.out.println("Nu e inceput de saptamana");
        }
    }
}
