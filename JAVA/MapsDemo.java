package java_week4;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapsDemo {
    public static void main(String[] args) {

        // Map<String, String> dex = new HashMap<>(); // random store + print

//        Map<String, String> dex = new LinkedHashMap<>(); //

        Map<String, String> dex = new TreeMap<>(); // alphabetical store + print

        // keys: masa, somn, cina
        // values: definitii
        dex.put("masa", " Mobilă formată dintr-o placă dreptunghiulară");
        dex.put("somn", "Stare fiziologică normală și periodică de repaus a ființelor");
        dex.put("cina", " A lua masa de seara.");

        // access the values from map
        System.out.println("Definitia pentru cina este: " + dex.get("cina"));
        System.out.println("Exista cuvantul somn in dictionar: " + dex.containsKey("somn")); // true

        System.out.println(dex.size()); // 3

        // return all the keys
        Set<String> keys = dex.keySet();
        System.out.println(keys);

        // return all the values
        Collection<String> values = dex.values();
        System.out.println(values);

        // the easy way
        System.out.println(dex.values());

        // return the all map
        System.out.println(dex.entrySet());

        // null in map -> null on values

        dex.put("automation", null);
        System.out.println(dex.entrySet());

        // null on keys
        dex.put(null, "no value present"); // the key cannot be null

        System.out.println(dex.entrySet());
        Set<Map.Entry<String, String>> entrySetDex = dex.entrySet();

        for (Map.Entry<String, String> perecheCuvantSiDefinitiaLui : entrySetDex) {
            System.out.println(perecheCuvantSiDefinitiaLui.getKey() + " = " + perecheCuvantSiDefinitiaLui.getValue());
        }
    }
}
