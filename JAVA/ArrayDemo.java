package java_week2;

import java.util.Arrays;

public class ArrayDemo {
    public static void main(String[] args) {
        int nota1 = 10;
        int nota2 = 11;
        int nota3 = 9;

        int[] note = new int[16]; //dimensiunea este 16 dar indexarea incepe de la 0
        note[0] = 6;
        note[1] = 10;
        note[2] = 11;
        note[3] = 9;
        //     [6, 10, 11, 9, 0, 0, 0, ...]
        //index 0,  1,  2, 3, 4, 5
//        System.out.println("Array-ul este: " + note[15]);
//
//        String[] numeCursanti = new String[]{"Maria", "Denis", "Cornel"};
//
//        boolean[] prezentaCurs = {false, true, true};
//        System.out.println("Dimensiunea array-ului este: " + numeCursanti.length);
//
//        System.out.println("Ultimul element este: " + numeCursanti[numeCursanti.length -1]);

//        for (int nota : note) {
//            System.out.println("Nota este: " + nota);
//        }
//        for (int i = 0; i < note.length; i++) {
//            System.out.println("Nota de pe pozitia " + i + " este " + note[i]);
//        }
        System.out.println("Afisare array folosind to string" + Arrays.toString(note));
        System.out.println("Sortare nume cursanti: " + Arrays.toString(note));

        }
}
