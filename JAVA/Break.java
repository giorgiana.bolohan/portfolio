package java_week3;

public class Break {
    public static void main(String[] args) {
        int a=0;
        for(a =0; a<5; a++) {
            System.out.println("La inceputul sirului numeric" + a);
            if (a == 3) {
                break; //daca se pune continue, se va printa si pentru valoarea 4
                //daca se pune return, se opreste dupa acest mesaj si nu mai afiseaza continuarea
            }
            System.out.println("La finalul sirului numeric " + a);
        }
        System.out.println("Dupa iesirea din for");

    }
}
