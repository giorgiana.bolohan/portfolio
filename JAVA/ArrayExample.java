package java_week3;

public class ArrayExample {
    public static void main(String[] args) {
        //declarare
        String [] employees;
        //initializare
        employees = new String[5]; //dimensiunea sirului de valori
        //asignarea de valori
        employees[0]="Ana";
        employees[1]="Maria";
        employees[2]="Alex";
        employees[3]="Sorin";
        employees[4]="George";

        int [] age;
        age= new int [5];
        age[0]=19;
        age[1]=30;
        age[2]=41;
        System.out.println("Varsta de pe pozitia 4 este: " + age[4]);

    }
}
