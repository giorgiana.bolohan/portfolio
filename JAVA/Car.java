//package java_week3;
//
//public class Car {
// public static void main (String[] args) {
//
//     //FIELD-URI (proprietati, caracteristici etc. )
//
//     String marca;
//     String culoare;
//     String model;
//
//     CarEngine motor;
//     CarWheels[] roti;
// }
//
//        //METODE (behaviour)
//
//        public void start() {
//            if (motor.putere < 100) {
//                System.out.println("Vruuum");
//            } else {
//                System.out.println("Vruuum, vrruum");
//            }
//        }
//
//
//        //metoda non-void > returneaza o valoare in back-end spre a fi preluata de front-end pentru afisare
//        public String Descriere() {
//            return "Brand: " + marca + " " + "Nuanta: " + culoare + " " + "Design: " + model;
//        }
//
//        public String DescriereAudi() {
//            return "Brand: " + marca + " " +  "Nuanta: " + culoare;
//        }
//
//}
