package java_week2;

import java.util.Arrays;

public class PrimitivesVsReferences {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {1, 2, 3};
        int[] arr3 = {2, 5, 7};
        int[] arr4 = arr3;
        System.out.println("arr3: " + Arrays.toString(arr3));
        System.out.println("arr4: " + Arrays.toString(arr4));
        arr4[2] = 8;
        System.out.println("arr3: " + Arrays.toString(arr3));
        System.out.println("arr4: " + Arrays.toString(arr4));

        System.out.println("arr1: " + arr1);
        System.out.println("arr2: " + arr2);
        System.out.println("arr3: " + arr3);
        System.out.println("arr4: " + arr4);

        System.out.println("arr1==arr2 ? "+(arr1==arr2)); //nu se compara elementele array-ului, ci se compara id-ul
        System.out.println("arr2==arr3 ? "+(arr2==arr3));
        System.out.println("arr3==arr4 ? "+(arr3==arr4));

        System.out.println("arr1 equals arr2 ? " +
                Arrays.equals(arr1, arr2));
        System.out.println("arr2 equals arr3 ? " +
                Arrays.equals(arr2, arr3));
        System.out.println("arr3 equals arr4 ? " +
                Arrays.equals(arr3, arr4));

        String s1 = "abc";

        String s2 = "abc";

        String s3 = "ABC".toLowerCase();
        String s4 = new String("abc");
        System.out.println("s1: " + s1);
        System.out.println("s2: " + s2);
        System.out.println("s3: " + s3);
        System.out.println("s1==s2 ? " + (s1==s2));
        System.out.println("s2==s3 ? " + (s2==s3));
        System.out.println("s1 equals s2? "+s1.equals(s2));
        System.out.println("s1 equals s2? "+s2.equals(s3)); System.out.println("s2 equals s3? "+s2.equals(s3));
    }
}
