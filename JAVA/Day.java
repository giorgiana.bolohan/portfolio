package java_week4;

public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY

    // == equals? // null
    // int = 0 // double = 0.0d float = 0.0f // null
}
