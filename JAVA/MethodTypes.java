package java_week1;

public class MethodTypes {

    //START METODA VOID - afiseaza/pastreaza un rezultat doar in consola

    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        int sum = a + b;

        System.out.println(sum);


    }

// START METODA NON-VOID - returneaza o valoare in proiect

    public static int sum(int a, int b) {
        int sum = a + b;

        return sum;
    }
}
