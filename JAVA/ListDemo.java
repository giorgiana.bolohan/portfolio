package java_week4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        // create new list // arrayList implementation
        List<String> firstNames = new ArrayList<>();

        // check the list size
        System.out.println("The size of list is: " + firstNames.size()); // 0
        System.out.println("The list is empty? " + firstNames.isEmpty()); // true

        // add elements to list
        firstNames.add("Diana");
        firstNames.add("Maria");
        firstNames.add("Ioana");
        firstNames.add("George");

        System.out.println("The size of list is: " + firstNames.size()); // 4
        System.out.println("The list is empty? " + firstNames.isEmpty()); // false

        // allows duplicates
        firstNames.add("Diana");
        System.out.println(firstNames);
        System.out.println("The size of list is: " + firstNames.size()); // 5

        // remove one person from the list
        firstNames.remove("Diana");
        System.out.println("The list after remove Diana is " + firstNames);

        // Get a specific name based on get and index/position
        System.out.println("Get the Ioana element : " + firstNames.get(1));

        // delete the entire list
        // firstNames.clear();
        System.out.println("The list is empty: " + firstNames);

        // List with Objects -> list of cities

        Oras oras1 = new Oras("BC", "Bacau", 173000L);
        Oras oras2 = new Oras("IS", "Iasi", 700000L);

        List<Oras> orase = new ArrayList<>();
        orase.add(oras1);
        orase.add(oras2);

        // remove city from the list
        //orase.remove(oras1);

        // get the index
        System.out.println("Indexul pentru oras2 este: " + orase.indexOf(oras2)); // 1
        System.out.println(orase);

        // Sorting on list
        Collections.sort(firstNames); //sorting
        System.out.println(firstNames);

        // for - each
        for (String firstName : firstNames) {
            if (firstName.equals("Diana"))
                System.out.println(firstName);

            // este ignorata
            System.out.println("Am incheiat if-ul");

        }
    }
}
