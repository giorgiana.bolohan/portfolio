package java_week4;

public class Recap {
    // Recap Java_Week1, Week2, Week3
    // math + String concatenation
    public static void main(String[] args) {
        int one = 1;
        String two = "2";
        System.out.print(2 + one + two + one + 1);

        // Answers:
        // a. 3211 - correct
        // b. 21211
        // c. 323
        // d. 7

        flowControl();
        circusValidation();
    }

    // flow control exercise
    public static void flowControl() {
        int i;
        for (i = 0; i < 2; i = i + 5) {
            if (i < 5)
                continue;
            System.out.println(i);
        }
        System.out.println(i);
    }
    // Answers:
    // a. 0, 5
    // b. 0, 5, 10
    // c. 10
    // d. 5 -- correct


    // OOP principles
    static class Cat extends Animal {
        // overloading - same method name, different parameters
        public void jump(int a) {
            System.out.println("Cat");
        }
    }

    static class Rabbit extends Animal {
        // override - same method name, same number of parameters
        public void jump() {
            System.out.println("Rabbit");
        }
    }

    public static void circusValidation() {
        Animal cat = new Cat();
        Rabbit rabbit = new Rabbit();
        cat.jump();
        rabbit.jump();
    }
}

//  What type of principles represents this code? // polymorphism // mostenire
// Answers:
// 1. Animal, Rabbit
// 2. Cat, Rabbit
// 3. Animal, Animal
// 4. None of the above

